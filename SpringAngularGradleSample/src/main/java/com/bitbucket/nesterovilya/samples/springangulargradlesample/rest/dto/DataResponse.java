package com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto;

/**
 * @author Ilya Nesterov
 */
public class DataResponse implements Response {

    /**
     * Объект ответа
     */
    private Object data;

    public DataResponse() {}

    public DataResponse(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
