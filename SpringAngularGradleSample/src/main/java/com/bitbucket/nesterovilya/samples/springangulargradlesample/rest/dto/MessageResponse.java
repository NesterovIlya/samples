package com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto;

/**
 * @author Ilya Nesterov
 */
public class MessageResponse implements Response {

    /**
     * Некоторое сообщение о проведении операции
     */
    private String message;

    public MessageResponse() {}

    public MessageResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
