package com.bitbucket.nesterovilya.samples.springangulargradlesample.service;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.CertificateNotFoundException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.FieldValidationException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.WrongNameFilterException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;

import java.util.List;

/**
 * @author Ilya Nesterov
 */
public interface CertificateService {

    /**
     * Выборка всех страховых полисов
     */
    List<Certificate> getAllCertificates();

    /**
     * Выборка всех страховых полисов фио владельцев которых начинается с указанной поисковой строки
     * @throws WrongNameFilterException генерируется при несоответствии поисковой строки заявленным требованиям.
     * Поисковая строка может содержать только латинские и кириллические буквы разного регистра и пробелы.
     */
    List<Certificate> getCertificatesByNameFilter(String filter) throws WrongNameFilterException;

    /**
     * Выборка страхового полиса по уникальному идентификатору
     * @throws CertificateNotFoundException генерируется, если ни одной записи с указанным идентификатором
     * в базе данных не найдено.
     */
    Certificate getCertificateByGuid(String guid) throws CertificateNotFoundException;

    /**
     * Обновление данных страхового полиса
     * @param updatedCertificate страховой полис с обновленными данными
     * @throws FieldValidationException генерируется при несоответствии обновляемой сущности валидационным проверкам.
     */
    void updateCertificate(Certificate updatedCertificate) throws FieldValidationException;

}
