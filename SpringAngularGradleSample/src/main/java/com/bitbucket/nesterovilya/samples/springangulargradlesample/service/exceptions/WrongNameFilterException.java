package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions;

/**
 * @author Ilya Nesterov
 */
public class WrongNameFilterException extends Exception {

    public WrongNameFilterException(String message) {
        super(message);
    }
}
