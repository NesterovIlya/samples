package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.mapper;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.util.Mapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author Ilya Nesterov
 */

@Mapper
public interface CertificateMapper {

    String CERTIFICATE_ATTRIBUTES = "guid, name, date_of_birth, gender, category";

    @Select("SELECT "+ CERTIFICATE_ATTRIBUTES + " FROM CERTIFICATES LIMIT 50")
    @Results(value = {
            @Result(property = "guid", column = "guid"),
            @Result(property = "name", column = "name"),
            @Result(property = "dateOfBirth", column = "date_of_birth"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "category", column = "category")
    })
    List<Certificate> selectCertificates();

    @Select("SELECT "+ CERTIFICATE_ATTRIBUTES + " FROM CERTIFICATES WHERE name LIKE #{nameFilter} LIMIT 50")
    @Results(value = {
            @Result(property = "guid", column = "guid"),
            @Result(property = "name", column = "name"),
            @Result(property = "dateOfBirth", column = "date_of_birth"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "category", column = "category")
    })
    List<Certificate> selectCertificatesByNameFilter(@Param("nameFilter") String nameFilter);

    @Select("SELECT "+ CERTIFICATE_ATTRIBUTES + " FROM CERTIFICATES WHERE guid = #{guid}")
    @Results(value = {
            @Result(property = "guid", column = "guid"),
            @Result(property = "name", column = "name"),
            @Result(property = "dateOfBirth", column = "date_of_birth"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "category", column = "category")
    })
    Certificate selectCertificateByGuid(@Param("guid") String guid);

    @Update("UPDATE CERTIFICATES SET name = #{certificate.name}, date_of_birth = #{certificate.dateOfBirth}, gender = #{certificate.gender}, category = #{certificate.category} WHERE guid = #{certificate.guid}")
    int updateCertificate(@Param("certificate") Certificate certificate);
}
