package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Категория водителей
 * @author Ilya Nesterov
 */
public enum Category {

    A("Мотоциклы"),
    A1("Легкие мотоциклы"),
    B("Легковые автомобили"),
    BE("Легковые автомобили с прицепом"),
    B1("Трициклы"),
    C("Грузовые автомобили"),
    CE("Грузовые автомобили с прицепом"),
    C1("Легкие грузовики"),
    C1E("Легкие грузовики с прицепом"),
    D("Автобусы"),
    DE("Автобусы с прицепом"),
    D1("Небольшие автобусы"),
    D1E("Небольшие автобусы с прицепом"),
    М("Мопеды"),
    Tm("Трамваи"),
    Tb("Троллейбусы");

    private String title;

    public static Map<String, String> valuesMap() {
        Map<String, String> map = new LinkedHashMap<>();
        for (Category category : Category.values()) {
            map.put(category.name(), category.getTitle());
        }
        return map;
    }

    Category(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

}
