package com.bitbucket.nesterovilya.samples.springangulargradlesample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * @author Ilya Nesterov
 */

@Configuration
@ComponentScan("com.bitbucket.nesterovilya.samples.springangulargradlesample.generator")
public class TestConfig {

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("db/create-schema.sql")
                .build();
    }
}
