package com.bitbucket.nesterovilya.samples.springangulargradlesample.generator;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations.Category;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations.Gender;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * @author Ilya Nesterov
 */
@Component
public class CertificateObjectGenerator {

    public Certificate generateCertificateForUpdate(){
        Certificate certificate = new Certificate();
        certificate.setGuid("0f8c2064-fb81-411c-91f7-1d7019c88a07");
        certificate.setName("Можайский Владислав Иванович");
        certificate.setDateOfBirth(Timestamp.valueOf("1945-12-31 00:00:00"));
        certificate.setGender(Gender.MALE);
        certificate.setCategory(Category.Tb);
        return certificate;
    }
}
