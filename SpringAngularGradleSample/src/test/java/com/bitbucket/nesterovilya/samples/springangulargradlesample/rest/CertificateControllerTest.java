package com.bitbucket.nesterovilya.samples.springangulargradlesample.rest;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.RootConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.TestConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.WebConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.generator.CertificateObjectGenerator;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.util.IntegrationTestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.annotation.Resource;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Ilya Nesterov
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class, WebConfig.class, TestConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:db/initial-data.xml")
public class CertificateControllerTest {


    @Resource
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CertificateObjectGenerator certificateObjectGenerator;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");

        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .addFilter(characterEncodingFilter,"/*")
                .build();
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificates() throws Exception {
        mockMvc.perform(get(CertificateRestAPI.ROOT)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificatesByFilter() throws Exception {
        mockMvc.perform(get(CertificateRestAPI.ROOT + CertificateRestAPI.GET_CERTIFICATES_BY_FILTER)
                .param("filter", "Можайский")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificateByGuid() throws Exception {
        mockMvc.perform(get(CertificateRestAPI.ROOT + "/7238e4c6-68e4-4a04-97b9-ff9a78bcca88")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/after-update-certificate.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdateCertificate() throws Exception {
        mockMvc.perform(put(CertificateRestAPI.ROOT)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(certificateObjectGenerator.generateCertificateForUpdate()))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }
}