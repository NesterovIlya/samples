(function() {
  'use strict';

  angular
    .module('webapp')
    .directive('sampleNavbar', sampleNavbar);

  /** @ngInject */
  function sampleNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {

      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(moment) {
      var vm = this;
    }
  }

})();
