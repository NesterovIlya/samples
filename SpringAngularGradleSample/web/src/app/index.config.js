(function() {
  'use strict';

  angular
    .module('webapp')
    .config(restangularConfig);

  /** @ngInject */
  function restangularConfig(RestangularProvider) {
    RestangularProvider.setBaseUrl("http://localhost:8086/spring-angular-sample");
  }

})();
